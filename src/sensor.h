#ifndef SENSOR_H
#define SENSOR_H

struct Sensor
{
    float volt;
    int resist;
    float temp_k;
    float temp_c;
    float temp_f;
};

#endif
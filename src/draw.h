#ifndef DRAW_H
#define DRAW_H

#include <U8g2lib.h>


void draw_string(char *msg, U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2);
void draw_IP(const char *ip, char const* ssid, U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2);
void draw_boot(U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2);


#endif
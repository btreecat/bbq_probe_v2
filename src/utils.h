#ifndef UTILS_H
#define UTILS_H
#include <esp_adc_cal.h>
#include <math.h>
#include <states.h>

float calc_voltage(int adc_value, esp_adc_cal_characteristics_t *adc_chars);
float calc_resistance(int r2, double v_in, float v_out);
float calc_temp_k(float R1, double Ash, double Bsh, double Csh);
float k_to_c(float temp_k);
float c_to_f(float temp_c);
int next_state(int current_state);



#endif
#ifndef STATES_H
#define STATES_H

enum STATE
{
    HEAD,
    APIP,
    LOCALIP,
    PROBE0,
    PROBE1,
    TAIL
};

enum ALARM {
    HEAD,
    BUZZ_ON,
    BUZZ_OFF,
    TAIL
};

#endif
#ifndef WEB_H
#define WEB_H

#include <sensor.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <Preferences.h>


namespace HTTP {
    //Set of Strings and String Templates to be used when building HTTP reponses
    static const char* http_status = "HTTP/1.1 200 OK";
    static const char* http_content_type = "Content-type:text/html";
    static const char* html_head_refresh = "<!DOCTYPE html><html><head><meta http-equiv='refresh' content='5' /><title>Smoke Master 9K</title></head>";
    static const char* html_head_static = "<!DOCTYPE html><html><head><title>Smoke Master 9K</title></head>";

    static const char* html_body_temps = "<body> Probe Temps:"
                                        "</br>"
                                        "<p>Probe 0: %.1f %s</p>"
                                        "<p>Probe 1: %.1f %s</p>"
                                        "<a href='/cfg'>Settings</a>"
                                        "</body>";

    static const char* html_body_cfg_head = "<body>"
                                        "<form action='/cfg' method='POST'>"
                                        "<input type='hidden' name='head' value='true'>";


    static const char* html_body_cfg_foot = "<input type='hidden' name='tail' value='true'>"
                                        "<input type='submit' value='Save'>"
                                        "</form>"
                                        "</br>"
                                        "<a href='/'>Temps</a>"
                                        "</body>";

    static const char* html_form_chk = "Temp in F:</br>"
                                        "<input type='checkbox' name='temp' %s></br>";
                        
    static const char* html_form_ssid = "WiFi Network:</br>"
                                        "<input type='text' name='ssid' placeholder='Your_WiFi' value='%s'></br>";

    
    static const char* html_form_pass = "WiFi Password:</br>"
                                        "<input type='password' name='pass' value='%s'></br>";

    static const char* html_foot = "</html>";

}

enum METHODS {
    GET,
    POST,
    OTHER
};

struct http_request {
    METHODS method;
    char uri[20];
    char body[256];
};

String connect_wifi(Preferences preferences);
void http_response(Sensor readings[], WiFiClient client);
void process_request(Sensor readings[], WiFiClient client, Preferences preferences);


#endif
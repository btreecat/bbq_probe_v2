#include <utils.h>

float calc_voltage(int adc_value, esp_adc_cal_characteristics_t *adc_chars)
{
    uint32_t mvolt = esp_adc_cal_raw_to_voltage(adc_value, adc_chars);
    return float(mvolt) / 1000.0;
}

float calc_resistance(int r2, double v_in, float v_out)
{
    float resist = ((r2 * v_in) / v_out) - r2;
    return resist;
}

float calc_temp_k(float R1, double Ash, double Bsh, double Csh)
{
    float k = 1 / (Ash + (Bsh * log(R1)) + (Csh * pow(log(R1), 3)));
    return k;
}

float k_to_c(float temp_k)
{
    return (temp_k - 273.15);
}

float c_to_f(float temp_c)
{
    return (temp_c * 9 / 5) + 32;
}

int next_state(int current_state)
{

    if (current_state == (TAIL - 1))
    {
        current_state = HEAD + 1;
    }
    else
    {
        current_state++;
    }
    return current_state;
}
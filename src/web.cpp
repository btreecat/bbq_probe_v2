#include <web.h>

String connect_wifi(Preferences preferences)
{
    // there is a saved network so we should try to connect
    int status = WL_IDLE_STATUS;
    int retry = 0;
    String local_IP;
    Serial.print("SSID: ");
    Serial.println(preferences.getString("ssid", "").c_str());
    // Serial.print("PASSWD: ");
    // Serial.println(preferences.getString("passwd", "").c_str());
    while (status != WL_CONNECTED && retry <= 5)
    {

        if (strcmp(preferences.getString("passwd", "").c_str(), "") != 0)
        {
            status = WiFi.begin(preferences.getString("ssid", "").c_str(), preferences.getString("passwd", "").c_str());
        }
        else
        {
            status = WiFi.begin(preferences.getString("ssid", "").c_str());
        }
        delay(1000);
        retry++;
        Serial.println("Waiting for WiFi Connection");
    }

    if (status == WL_CONNECTED)
    {
        Serial.println("WiFi Connected");
        local_IP = WiFi.localIP().toString();
    }
    else
    {
        Serial.println("WiFi Connection Failed");
        local_IP = "";
        // WiFi.disconnect();

    }
    return local_IP;
}

void uri_root(Sensor readings[], WiFiClient client, Preferences preferences)
{
    preferences.begin("bbq", false);
    char payload[128];
    // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
    // and a content-type so the client knows what's coming, then a blank line:
    // Serial.write("Sending Reply\n");
    client.println(HTTP::http_status);
    client.println(HTTP::http_content_type);
    client.println();

    // the content of the HTTP response follows the header:
    client.println(HTTP::html_head_refresh);
    if (preferences.getBool("fahrenheit", false))
    {
        sprintf(payload, HTTP::html_body_temps, readings[0].temp_f, "F", readings[1].temp_f, "F");
    }
    else
    {
        sprintf(payload, HTTP::html_body_temps, readings[0].temp_c, "C", readings[1].temp_c, "C");
    }

    client.print(payload);
    client.println(HTTP::html_foot);

    // The HTTP response ends with another blank line:
    client.println();
    preferences.end();
}

void uri_cfg(WiFiClient client, Preferences preferences)
{
    preferences.begin("bbq", false);
    char payload[128];
    // Serial.write("Sending Reply\n");
    client.println(HTTP::http_status);
    client.println(HTTP::http_content_type);
    client.println();

    client.println(HTTP::html_head_static);
    client.println(HTTP::html_body_cfg_head);
    if (preferences.getBool("fahrenheit", false))
    {
        sprintf(payload, HTTP::html_form_chk, "checked");
        client.print(payload);
    }
    else
    {
        sprintf(payload, HTTP::html_form_chk, "");
        client.print(payload);
    }

    sprintf(payload, HTTP::html_form_ssid, preferences.getString("ssid", "").c_str());
    client.print(payload);

    sprintf(payload, HTTP::html_form_pass, preferences.getString("passwd", "").c_str());
    client.print(payload);

    client.println(HTTP::html_body_cfg_foot);
    client.println(HTTP::html_foot);

    // The HTTP response ends with another blank line:
    client.println();
    preferences.end();
}

void parse_body(char body[], Preferences preferences)
{
    preferences.begin("bbq", false);
    String currentLine = String(body);
    bool WIFI_CHANGED = false;

    if (currentLine.indexOf("&temp=on") != -1)
    {
        Serial.println("Temp in deg F");
        preferences.putBool("fahrenheit", true);
    }
    else
    {
        Serial.println("Temp in deg C");
        preferences.putBool("fahrenheit", false);
    }
    if (currentLine.indexOf("&ssid=") != -1)
    {
        int space_start = currentLine.indexOf("&ssid=") + 6;
        int space_end = currentLine.indexOf("&", space_start);
        String ssid = currentLine.substring(space_start, space_end);
        ssid.replace("+", " ");
        if (strcmp(ssid.c_str(), preferences.getString("ssid", "").c_str()) != 0)
        {
            //detected a change in the SSID
            preferences.putString("ssid", ssid);
            WIFI_CHANGED = true;
        }
        // Serial.print("SSID: ");
        // Serial.println(ssid);
    }
    if (currentLine.indexOf("&pass=") != -1)
    {
        int space_start = currentLine.indexOf("&pass=") + 6;
        int space_end = currentLine.indexOf("&", space_start);
        String pass = currentLine.substring(space_start, space_end);
        pass.replace("+", " ");

        if (strcmp(pass.c_str(), preferences.getString("passwd", "").c_str()) != 0)
        {
            preferences.putString("passwd", pass);
            WIFI_CHANGED = true;
        }
        Serial.print("pass: ");
        Serial.println(pass);
    }

    if (WIFI_CHANGED)
    {
        String local_IP = connect_wifi(preferences);
        preferences.putString("local_IP", local_IP);
    }
    preferences.end();
}

void process_request(Sensor readings[], WiFiClient client, Preferences preferences)
{
    http_request request;

    //Lets set the default URI and METHOD to / and GET respectively
    strcpy(request.uri, "/");
    request.method = GET;

    String currentLine = ""; // make a String to hold incoming data from the client

    while (client.available())
    {                           // if there's bytes to read from the client,
        char c = client.read(); // read a byte, then
        Serial.write(c);        // print it out the serial monitor

        if (c == '\n')
        { // if the byte is a newline character, the line *should be* finished and we can try to parse it

            // Serial.println("----------------------------");

            if (currentLine.indexOf("GET /") != -1)
            {
                request.method = GET;
            }
            else if (currentLine.indexOf("POST /") != -1)
            {
                request.method = POST;
            }

            //detect uri
            if (((currentLine.indexOf("GET /") != -1) || (currentLine.indexOf("POST /") != -1)) && (currentLine.indexOf("HTTP/1.1") != -1))
            {
                int space_start = currentLine.indexOf(" ") + 1;
                int space_end = currentLine.lastIndexOf(" ");
                String uri = currentLine.substring(space_start, space_end);
                strcpy(request.uri, uri.c_str());
            }

            //The only char is a \n so this line is blank;
            if (currentLine.length() != 0)
            {
                currentLine = "";
            }
        }
        else if (c != '\r')
        {                     // if you got anything else but a carriage return character,
            currentLine += c; // add it to the end of the currentLine
        }
    }

    //The last line is still held in currnt line. Check if we can find our form data.
    if (currentLine.indexOf("&tail=true") != -1)
    {
        Serial.println();

        strcpy(request.body, currentLine.c_str());
    }

    if (request.uri)
    {

        if (request.method == GET && strcmp(request.uri, "/") == 0)
        {
            //root
            // Serial.println("ROOT URI, GET METHOD");
            uri_root(readings, client, preferences);
        }
        else if (request.method == GET && strcmp(request.uri, "/cfg") == 0)
        {
            //show config page
            // Serial.println("CFG URI, GET METHOD");
            uri_cfg(client, preferences);
        }
        else if (request.method == POST && strcmp(request.uri, "/cfg") == 0)
        {
            //save config
            // Serial.println("CFG URI, POST METHOD");
            // uri_root(readings, client);
            parse_body(request.body, preferences);
            uri_cfg(client, preferences);
        }
        else
        {
            //no idea what was tried, just return root
            // uri_root(readings, client);
            Serial.println("UNKNOWN URI OR UNKNOWN METHOD");
            Serial.print("**URI:");
            Serial.println(request.uri);
            Serial.print("**METHOD:");
            Serial.println(request.method);
            client.stop();
        }
    }
}
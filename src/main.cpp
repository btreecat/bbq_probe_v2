#include <Arduino.h>
#include <esp_adc_cal.h>
#include <WiFi.h>
#include <WiFiAP.h>
#include <string>
#include <web.h>
#include <config.h>
#include <utils.h>
#include <draw.h>

using namespace std;

U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2(U8G2_R0, 4, 5);

Preferences preferences;

int state_display = HEAD + 1;
int state_buzz = HEAD + 1;

// esp_err_t status = adc2_vref_to_gpio(GPIO_NUM_26);
// esp_err_t status2 = adc2_vref_to_gpio(GPIO_NUM_25);

static esp_adc_cal_characteristics_t *adc_chars;

String client_ssid = "";
String client_pass = "";
bool fahrenheit = false;
WiFiServer server(80);
long time_screen = 0;
long time_probe = 0;
long time_wifi = 0;
long time_buzz = 0;
Sensor readings[PROBES];

int *read_pins()
{
    static int pins[2];
    int smp_ch1 = 0;
    int smp_ch2 = 0;
    for (int i = 0; i < SAMPLES; i++)
    {
        smp_ch1 += adc1_get_raw(channel_0);
        smp_ch2 += adc1_get_raw(channel_1);
    }
    int avg_ch1 = smp_ch1 / SAMPLES;
    int avg_ch2 = smp_ch2 / SAMPLES;
    pins[0] = avg_ch1;
    pins[1] = avg_ch2;
    return pins;
}

void setup(void)
{
    Serial.begin(115200);
    pinMode(GPIO_NUM_16, OUTPUT);

    U8G2.begin();
    draw_boot(U8G2);
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(channel_0, atten);
    adc1_config_channel_atten(channel_1, atten);
    adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
    // esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, V_REF, adc_chars);
    esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, V_REF, adc_chars);

    //Start AP
    // You can remove the password parameter if you want the AP to be open.
    preferences.begin("bbq", false);
    WiFi.softAP(AP_SSID);
    preferences.putString("ap_IP", WiFi.softAPIP().toString());
    server.begin();

    if (strcmp(preferences.getString("ssid", "").c_str(), "") != 0)
    {
        preferences.putString("local_IP", connect_wifi(preferences));
    }
    preferences.end();
}

void loop(void)
{

    long time_current = millis();
    long time_diff_screen = time_current - time_screen;
    long time_diff_probe = time_current - time_probe;
    long time_diff_wifi = time_current - time_wifi;
    long time_diff_buzz = time_current - time_buzz;

    char msg[10];
    int *sensors = read_pins();

    if (time_diff_probe >= LOOP_PROBE)
    {
        for (int i = 0; i < PROBES; i++)
        {
            Sensor reading;
            reading.volt = calc_voltage(sensors[i], adc_chars);
            reading.resist = calc_resistance(R2, V_IN, reading.volt);
            reading.temp_k = calc_temp_k(reading.resist, Ash, Bsh, Csh);
            reading.temp_c = k_to_c(reading.temp_k);
            reading.temp_f = c_to_f(reading.temp_c);
            readings[i] = reading;
        }
        time_probe = time_current;
    }

    WiFiClient client = server.available(); // listen for incoming clients
    if (client.connected())
    {
        process_request(readings, client, preferences);
        delay(10); //give the client a moment to process the conetent.
    }

    if (time_diff_wifi >= LOOP_WIFI)
    {
        //Let's check if we are still connected. If not, retry connection
        preferences.begin("bbq", false);
        if (strcmp(preferences.getString("local_IP", "").c_str(), "") == 0 || WiFi.status() != WL_CONNECTED)
        {
            // Serial.println("WiFi Discconeccted, Attempting Connection");
            preferences.putString("local_IP", connect_wifi(preferences));
        }
        preferences.end();
        time_wifi = time_current;
    }

    if (time_diff_screen >= LOOP_SCREEN)
    {
        preferences.begin("bbq", false);

        if (state_display == APIP)
        {
            //update display with current IP of the AP along with AP SSID
            if (strcmp(preferences.getString("ap_IP", "").c_str(), "") != 0)
            {
                draw_IP(preferences.getString("ap_IP", "").c_str(), AP_SSID, U8G2);
            }
        }
        else if (state_display == LOCALIP)
        {
            if (WiFi.status() == WL_CONNECTED)
            {
                draw_IP(preferences.getString("local_IP", "").c_str(), preferences.getString("ssid", "").c_str(), U8G2);
            }
            else
            {
                //Lets go ahead and cheat the time so we can skip this screen
                time_current -= LOOP_SCREEN;
            }
        }

        else if (state_display == PROBE0)
        {
            //update the display with the first probe
            if (preferences.getBool("fahrenheit", false))
            {
                sprintf(msg, "Tf_%d: %.1f", 0, readings[0].temp_f);
            }
            else
            {
                sprintf(msg, "Tc_%d: %.1f", 0, readings[0].temp_c);
            }

            draw_string(msg, U8G2);
        }
        else if (state_display == PROBE1)
        {
            //update the display with the second probe
            if (preferences.getBool("fahrenheit", false))
            {
                sprintf(msg, "Tf_%d: %.1f", 1, readings[1].temp_f);
            }
            else
            {
                sprintf(msg, "Tc_%d: %.1f", 1, readings[1].temp_c);
            }

            draw_string(msg, U8G2);
        }

        //advanced the state to the next one
        state_display = next_state(state_display);
        time_screen = time_current;
        preferences.end();
    }

    // if (time_diff_buzz >= LOOP_BUZZ)
    // {
    //     preferences.begin("bbq", false);
    //     //if Probe 0 temp is 
    //     if (state_buzz == BUZZ_ON) {
    //         digitalWrite(GPIO_NUM_16, HIGH);
    //         time_buzz -= (LOOP_BUZZ/2);
    //     }
    //     else {
    //         digitalWrite(GPIO_NUM_16, LOW);
    //         time_buzz = time_current;
    //     }
    //     state_buzz = next_state(state_buzz);
    //     preferences.end();
    // }

}

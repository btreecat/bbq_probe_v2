#ifndef CONFIG_H
#define CONFIG_H

#include <driver/adc.h>

// static float V_ADC = 3.20;
static const float V_IN = 3.3;
static const int R2 = 10000;
//These avlues are probe dependent
static const double Ash = 0.00073431401;
static const double Bsh = 0.00021574370;
static const double Csh = 0.000000095156860;
//This value is ESP32 dependent
static const int V_REF = 1139;
static const int PROBES = 2;
static const int SAMPLES = 42;
// static int REF_V = 1100;
// static int ADC_HIGH = 4095;
static const adc1_channel_t channel_0 = ADC1_CHANNEL_3;
static const adc1_channel_t channel_1 = ADC1_CHANNEL_0;
static const adc_atten_t atten = ADC_ATTEN_DB_11;
static const adc_unit_t unit = ADC_UNIT_1;
static const char *AP_SSID = "BBQ_Probe";
static const int LOOP_SCREEN = 3000;
static const int LOOP_PROBE = 500;
static const int LOOP_WIFI = 30000;
static const int LOOP_BUZZ = 500;

#endif
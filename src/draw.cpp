#include <draw.h>

void draw_string(char *msg, U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2)
{
    U8G2.clearBuffer();
    U8G2.setFont(u8g2_font_ncenB14_tr);
    U8G2.drawStr(0, 20, msg);
    U8G2.sendBuffer();
}

void draw_IP(const char *ip, char const*ssid, U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2)
{
    char ap[20];
    U8G2.clearBuffer();
    U8G2.setFont(u8g2_font_ncenB12_tr);
    sprintf(ap, "%s:", ssid);
    U8G2.drawStr(0, 20, ap);
    U8G2.drawStr(0, 50, ip);
    U8G2.sendBuffer();
}


void draw_boot(U8G2_SSD1306_128X64_NONAME_F_SW_I2C U8G2) 
{
        U8G2.clearBuffer();
        U8G2.setFont(u8g2_font_ncenB12_tr);
        U8G2.drawStr(40, 20, "Smoke");
        U8G2.drawStr(20, 40, "Master 9K");
        U8G2.setFont(u8g2_font_ncenB08_tr);
        U8G2.drawStr(300, 58, "booting...");
        U8G2.sendBuffer();

}